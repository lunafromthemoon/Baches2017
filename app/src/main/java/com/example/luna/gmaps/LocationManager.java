package com.example.luna.gmaps;

import android.location.Location;
import android.os.Looper;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

/**
 * Created by luna on 10/17/17.
 */

public class LocationManager {

    private static final String TAG = "Location Manager";
    private static final String NO_ID = "NOID";

    private MapsActivity parent;
    private StorageManager storageManager;
    private AccelerometerManager accelerometerManager;
    private EventsManager eventsManager;

    private int trackId;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest mLocationRequest;

    public Location getCurrentLocation() {
        return currentLocation;
    }

    private Location currentLocation = null;
    private SettingsClient mSettingsClient;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;

    private int updatesCounter = 0;
    private Location startLocation = null;
    private Location endLocation = null;

    public LocationManager(MapsActivity parent,StorageManager storageManager, AccelerometerManager accelerometerManager,EventsManager eventsManager){
        this.parent = parent;
        this.eventsManager = eventsManager;
        this.storageManager = storageManager;
        this.accelerometerManager = accelerometerManager;
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this.parent);
        mSettingsClient = LocationServices.getSettingsClient(this.parent);
        createLocationCallback();
        createLocationRequest();
        buildLocationSettingsRequest();

    }

    public FusedLocationProviderClient getmFusedLocationClient() {
        return mFusedLocationClient;
    }

    private void createLocationCallback() {
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                Log.i(TAG,"Getting location update.");
                Location newLocation = locationResult.getLastLocation();
                if (currentLocation != null){
                    //create location range id
                    EventsLocationRange event = eventsManager.closeEventRange(currentLocation,newLocation);
                    List<String> rangeData = DataParser.convertTrack(updatesCounter,event);
                    storageManager.addData("track"+trackId,rangeData);
                }
                currentLocation = newLocation;
                if (startLocation == null){
                    startLocation = currentLocation;
                }
                updatesCounter++;
                accelerometerManager.setLocationUpdatesCounter(updatesCounter);

                parent.showLocation(currentLocation.getLatitude(), currentLocation.getLongitude());

            }
        };
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    public static Location getMidPoint(Location l1, Location l2){
        return getMidPoint(l1.getLatitude(),l1.getLongitude(),l2.getLatitude(),l2.getLongitude());
    }

    public static Location getMidPoint(double lat1,double lon1, double lat2, double lon2){
        double lat3 = (lat1+lat2)/2;
        double lon3 = (lon1+lon2)/2;
        Location l3 = new Location("");
        l3.setLatitude(lat3);
        l3.setLongitude(lon3);
        return  l3;
    }

    public static LatLng getLatLng(Location location){
        return new LatLng(location.getLatitude(),location.getLongitude());
    }

    public void startRecording(int trackId){
        this.trackId = trackId;
        updatesCounter = 0;
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(parent, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");
                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());

                    }
                });
    }

    public void stopRecording(){
        if (startLocation == null){
            cancelRecording();
            return;
        }
        endLocation = currentLocation;
        //saveTrackDirectory();
        currentLocation = null;
        //storageManager.addHeader(startLocation,endLocation);
        storageManager.write("track"+trackId);
        Log.i(TAG,"Stopping location updates.");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    public void cancelRecording(){
        currentLocation = null;
        storageManager.resetData();
        Log.i(TAG,"Cancelling location updates.");
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }

}
