package com.example.luna.gmaps;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by luna on 2/5/18.
 */

public class TrackInfo {

    private static final String PREFIX = "track";
    private static final DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

    private String name;
    private int id;
    private String city;
    private Date date;
    private boolean uploaded = false;


    public TrackInfo(int trackNumber,String city){
        this.id = trackNumber;
        this.name = PREFIX+trackNumber;
        this.date = new Date();
        this.city = city;
    }

    public TrackInfo(String csvLine){
        String[] d = csvLine.split(";");
        this.name = d[0];
        this.id = Integer.valueOf(this.name.replace(PREFIX,""));
        this.city = d[1];
        try {
            this.date = format.parse(d[2]);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.uploaded = Boolean.valueOf(d[3]);
    }

    public String toString(){
        return this.name+";"+this.city+";"+format.format(this.date)+";"+this.uploaded;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }
}
