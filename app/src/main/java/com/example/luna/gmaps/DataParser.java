package com.example.luna.gmaps;

import android.location.Location;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luna on 12/7/17.
 */

public class DataParser {

    public final static String STABILITY_EVENT = "Stability Event";

    public static EventsLocationRange convertEvent(JsonObject eObject){
        double startLat = eObject.get("start").getAsJsonObject().get("latitude").getAsDouble();
        double startLon = eObject.get("start").getAsJsonObject().get("longitude").getAsDouble();
        Location startLocation = new Location("");
        startLocation.setLatitude(startLat);
        startLocation.setLongitude(startLon);
        // end location
        double endLat = eObject.get("end").getAsJsonObject().get("latitude").getAsDouble();
        double endLon = eObject.get("end").getAsJsonObject().get("longitude").getAsDouble();
        Location endLocation = new Location("");
        endLocation.setLatitude(endLat);
        endLocation.setLongitude(endLon);
        EventsLocationRange event = new EventsLocationRange(startLocation,endLocation);
        event.setId(eObject.get("id").getAsString());
        event.setReadingDateTime(eObject.get("date").getAsLong());
        event.setScore(eObject.get("score").getAsDouble());
        event.setAccuracy(eObject.get("accuracy").getAsInt());
        return event;
    }

    public static String convertAccelerometerData(float[] raw,float[] delta, float[] diff, String axis, int id,int eventId){
        return id + ";" + eventId + ";" + System.currentTimeMillis() + ";"
                + raw[0] + ";"
                + delta[0] + ";"
                + diff[0] + ";"
                + raw[1] + ";"
                + delta[1] + ";"
                + diff[1] + ";"
                + raw[2] + ";"
                + delta[2] + ";"
                + diff[2] + ";"
                + axis;
    }

    public static String convertStabilityEvent(StabilityEvent e){
        return  STABILITY_EVENT+";" //0
                + e.getId() + ";" //1
                + e.getStartTime() + ";" //2
                + e.getEndTime() + ";" //3
                + e.getxAvg() + ";" //4
                + e.getyAvg() + ";" //5
                + e.getzAvg() + ";" //6
                + e.getScore() + ";"
                + e.getDuration();
    }

    public static List<String> convertTrack(int updatesCounter, EventsLocationRange e){
        String header = updatesCounter+ ";" //0
                + e.getReadingDateTime() +";" //1
                + e.getStartLocation().getLatitude() + ";" //2
                + e.getStartLocation().getLongitude() + ";" //3
                + e.getEndLocation().getLatitude()+";" //4
                + e.getEndLocation().getLongitude()+";" //5
                + e.getEndLocation().getSpeed()+";" //6
                + e.getRangeDistance() + ";"
                + e.getScore();
        List<String> list = new ArrayList<String>();
        list.add(header);
        for (StabilityEvent event : e.getEvents()){
            list.add(convertStabilityEvent(event));
        }
        return list;
    }

    public static EventsLocationRange convertTrack(String line){
        String[] d = line.split(";");
        Location start = new Location("");
        start.setLatitude(Double.parseDouble(d[2]));
        start.setLongitude(Double.parseDouble(d[3]));
        Location end = new Location("");
        end.setLatitude(Double.parseDouble(d[4]));
        end.setLongitude(Double.parseDouble(d[5]));
        EventsLocationRange e = new EventsLocationRange("",start,end);
        e.setReadingDateTime(Long.parseLong(d[1]));
        return e;
    }

    public static StabilityEvent convertStabilityEvent(String line){
        String[] d = line.split(";");
        StabilityEvent e = new StabilityEvent(Integer.parseInt(d[1]));
        e.setStartTime(Long.parseLong(d[2]));
        e.setEndTime(Long.parseLong(d[3]));
        e.setxAvg(Float.parseFloat(d[4]));
        e.setyAvg(Float.parseFloat(d[5]));
        e.setzAvg(Float.parseFloat(d[6]));
        return e;
    }


}
