package com.example.luna.gmaps;

import android.location.Location;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luna on 12/8/17.
 */

public class EventsManager {

    private MapsActivity parent;
    private String userId;
    MediaPlayer sound;
    private List<EventsLocationRange> nearbyEvents;
    private StabilityEvent event = null;
    private int eventCounter = 0;
    private List<StabilityEvent> events = new ArrayList<StabilityEvent>();

    public EventsManager(MapsActivity parent){
        this.parent = parent;
        this.userId = parent.getUserId();
        sound = MediaPlayer.create(parent, R.raw.blip);
    }

    public void checkEvent(float[] delta,boolean isStable){
        if (event == null){
            //start event logging
            event = new StabilityEvent(eventCounter);
            eventCounter++;
            event.update(delta[0],delta[1],delta[2]);
            Toast.makeText(parent, "Recording event!", Toast.LENGTH_SHORT).show();
            sound.start();
        } else {
            if (isStable){
                event.close();
                events.add(event);
                event = null;
            } else {
                event.update(delta[0],delta[1],delta[2]);
            }
        }
    }

    public boolean eventIsOpen(){
        return event!=null;
    }

    public EventsLocationRange closeEventRange( Location startLocation, Location endLocation){
        //create event
        Log.i("Events Manager","Creating event with "+events.size()+" registered events.");
        EventsLocationRange e = new EventsLocationRange(startLocation,endLocation);
        e.setEvents(events);
        events = new ArrayList<>();
        return e;
    }

    public List<EventsLocationRange> parseTrack(List<String> trackData){
        List<EventsLocationRange> track = new ArrayList<>();
        EventsLocationRange e = null;
        for (String d : trackData){
            if (d.startsWith(DataParser.STABILITY_EVENT) && e!=null){
                e.addEvent(DataParser.convertStabilityEvent(d));
            } else {
                e = DataParser.convertTrack(d);
                track.add(e);
            }
        }
        return track;
    }

    public void startRecording(){
        event = null;
        eventCounter = 0;
        events = new ArrayList<>();
    }

    public void stopRecording(){
        event = null;
        events = null;
    }

    public static Location getNextLocation(EventsLocationRange event,Location previous){
        Location start = event.getStartLocation();
        Location end = event.getEndLocation();
        if (start.getLongitude() == previous.getLongitude() && start.getLongitude() == previous.getLongitude()){
            // event start location equals previous location, next location is end location
            return end;
        }
        if (end.getLongitude() == previous.getLongitude() && end.getLongitude() == previous.getLongitude()){
            // event end location equals previous location, next location is start location
            return start;
        }
        // location is not continuous, check which is closer
        if (start.distanceTo(previous) < end.distanceTo(previous)){
            //event start location is closer in distance to the previous location, next location is end
            return end;
        } else {
            //event end location is closer in distance to the previous location, next location is start
            return start;
        }
    }

    public void parseNearbyEvents(JsonObject object){
        nearbyEvents = new ArrayList<>();
        System.out.println(object);
        JsonArray data = object.getAsJsonArray("data");
        for (JsonElement element : data){
            JsonObject eObject = element.getAsJsonObject();
            nearbyEvents.add(DataParser.convertEvent(eObject));
        }
        Log.i("EventsManager","Found "+nearbyEvents.size()+" events.");
        parent.showNearbyEvents(1);
    }

    public List<EventsLocationRange> getNearbyEvents() {
        return nearbyEvents;
    }

    public int getEventId() {
        return event == null ? -1 : event.getId();
    }
}
