package com.example.luna.gmaps;

import android.app.Activity;
import android.location.Location;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v4.content.ContextCompat;
import android.util.FloatProperty;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by luna on 8/10/17.
 */

public class StorageManager {

    private boolean canWrite = false;
    private MapsActivity parent;

    private Map<String,List<String>> allData;

    public StorageManager(MapsActivity parent){
        this.parent = parent;
        resetData();
        canWrite = isExternalStorageWritable();

    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return canWrite || parent.canWriteToSD();
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public void resetData(){
        allData = new HashMap<String, List<String>>();
    }

    public void addData(String file, List<String> lines ){
        List<String> d = allData.get(file);
        if (d==null){
            d = new ArrayList<String>();
            allData.put(file,d);
        }
        d.addAll(lines);
        if (d.size() > 900){
            write(file);
        }
    }

    public void addData(String file,String line){
        List<String> d = allData.get(file);
        if (d==null){
            d = new ArrayList<String>();
            allData.put(file,d);
        }
        d.add(line);
        if (d.size() > 900){
            write(file);
        }
    }


    public List<String> read(String name){
        List<String> track = new ArrayList<String>();
        boolean canRead = isExternalStorageReadable();
        if (!canRead){
            return null;
        }
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/baches2017");
        dir.mkdirs();
        File file = new File(dir, name+".csv");
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                if (line.length() < 500) { //garbage!
                    track.add(line);
                }
            }
            br.close();
        }
        catch (IOException e) {
            Log.i("StorageManager","File "+name+".csv not found.");
            //e.printStackTrace();
        }
        return track;
    }

    public void write(String file){
        List<String> d = allData.get(file);
        write(d,true,file);
        allData.remove(file);
    }

    public void write(List<String> toWrite, boolean append,String name){
        canWrite = isExternalStorageWritable();
        if (!canWrite || toWrite == null || toWrite.size() < 1){
            return;
        }
        File root = android.os.Environment.getExternalStorageDirectory();
        File dir = new File(root.getAbsolutePath() + "/baches2017");
        dir.mkdirs();
        File file = new File(dir, name+".csv");
        try {
            FileOutputStream f = new FileOutputStream(file,append);
            PrintWriter pw = new PrintWriter(f);
            Log.i("StorageManager","Saving "+toWrite.size()+" lines.");
            for (String s : toWrite){
                pw.println(s);
            }
            pw.flush();
            pw.close();
            f.close();
            Log.i("StorageManager","File written: "+file.getName());
//            Toast.makeText(parent, "File written: "+file.getName(), Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
