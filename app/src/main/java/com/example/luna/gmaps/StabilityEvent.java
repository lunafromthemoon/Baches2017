package com.example.luna.gmaps;

/**
 * Created by luna on 12/6/17.
 */

public class StabilityEvent {

    private int id;
    private long startTime;
    private long endTime;
    private float xAvg = 0;
    private float yAvg = 0;
    private float zAvg = 0;
    private int updatesCounter = 0;

    public StabilityEvent(int id){
        this.id = id;
        startTime = System.currentTimeMillis();
    }

    public void update(float x, float y, float z){
        xAvg += x;
        yAvg += y;
        zAvg += z;
        updatesCounter++;
    }

    public void close(){
        updatesCounter -= (AccelerometerManager.STBTOLERANCE-1);
        xAvg = xAvg/updatesCounter;
        yAvg = yAvg/updatesCounter;
        zAvg = zAvg/updatesCounter;
        endTime = System.currentTimeMillis();
    }

    public double getScore(){
        return (xAvg+yAvg+zAvg)*getDuration();
    }

    public double getDuration() {
        return Double.valueOf(endTime-startTime)/1000;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public float getxAvg() {
        return xAvg;
    }

    public void setxAvg(float xAvg) {
        this.xAvg = xAvg;
    }

    public float getyAvg() {
        return yAvg;
    }

    public void setyAvg(float yAvg) {
        this.yAvg = yAvg;
    }

    public float getzAvg() {
        return zAvg;
    }

    public void setzAvg(float zAvg) {
        this.zAvg = zAvg;
    }

    public int getUpdatesCounter() {
        return updatesCounter;
    }

    public void setUpdatesCounter(int updatesCounter) {
        this.updatesCounter = updatesCounter;
    }
}
