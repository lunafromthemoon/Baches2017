package com.example.luna.gmaps;

/**
 * Created by luna on 12/7/17.
 */

public interface Scorable {

    public double getScore();
}
