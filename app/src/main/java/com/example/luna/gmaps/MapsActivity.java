package com.example.luna.gmaps;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.location.Location;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,SensorEventListener {

    private static final String TAG = MapsActivity.class.getSimpleName();
    private static final String LAST_TRACK = "LastTrack";
    private static final String TRACKS_FILE = "tracks_info";
    private static final String USER_ID = "UserID";
    static final int PERMISSIONS_REQUEST_CODE = 3;
    static final int MAX_TRACKS = 5;

    private String userId;


    private GoogleMap mMap = null;
    private ToggleButton toggleButton;
    private Button cancelButton;
    private CheckBox saveRawDataCheckbox;
    private List<String> items;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private Spinner accuracyLevelText;

    private List<TrackInfo> tracks = new ArrayList<>();
    private TrackInfo lastTrack;
    private TrackInfo currentTrack;
    private SharedPreferences sharedPref;


    private AccelerometerManager accelerometerManager;
    private StorageManager storageManager;
    private LocationManager locationManager;
    private EventsManager eventsManager;
    private RestClient restClient;
    private String currentCity = "";

    private EventScoreScale scale;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //request permissions
        requestPermissions();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        //load UI
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        toggleButton = (ToggleButton) findViewById(R.id.recordButton);
        cancelButton = (Button) findViewById(R.id.cancel);
        saveRawDataCheckbox = (CheckBox) findViewById(R.id.saveRawData);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.ldrawer);
        accuracyLevelText = (Spinner) findViewById(R.id.accSpinner);
        addAccuracyChangeListener();
        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        loadUserId();

        //init storage
        storageManager = new StorageManager(this);
        eventsManager = new EventsManager(this);
        restClient = new RestClient(this,eventsManager);
//        potholes = storageManager.loadEvents();
        readTracksFile();
        initSideMenu();
        //init accelerometer
        accelerometerManager = new AccelerometerManager(this,storageManager,eventsManager);
        //init location updates
        locationManager = new LocationManager(this,storageManager,accelerometerManager,eventsManager);

    }

    public void loadUserId(){
        userId = sharedPref.getString(USER_ID, null);
        if (userId == null) {
            userId = UUID.randomUUID().toString();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(USER_ID, userId);
            editor.commit();
        }
    }

    public void readTracksFile(){
        List<String> trackList = storageManager.read(TRACKS_FILE);
        if (trackList.size() == 0){
            return;
        }
        for (String line : trackList){
            tracks.add(new TrackInfo(line));
        }
        lastTrack = tracks.get(tracks.size()-1);
    }

    public void saveTracksFile(){
        List<String> trackList = new ArrayList<>();
        for (TrackInfo track : tracks){
            Log.i(TAG,"Track "+track.getId()+" uploaded "+track.isUploaded());
            trackList.add(track.toString());
        }
        storageManager.write(trackList,false,TRACKS_FILE);
    }

    private void addAccuracyChangeListener(){
        final MapsActivity parent = this;
        final String[] options = new String[]{"Acc. 1", "Acc. 2", "Acc. 3", "Acc. 5", "Acc. 8", "Acc. 10", "Acc. 15"};
        ArrayAdapter<String> aa = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, options);
        //aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accuracyLevelText.setAdapter(aa);
        accuracyLevelText.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                String nb = options[position].replace("Acc. ","");
                Integer accLevel = Integer.valueOf(nb);
                System.out.println("acc level "+accLevel);
                parent.showNearbyEvents(accLevel);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    public String getUserId(){
        return userId;
    }


    public void initSideMenu(){
        initSidepanelItems();
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = items.get(position);
                switch (item){
                    case "Test Server" :
                        Toast.makeText(MapsActivity.this, "Testing Server", Toast.LENGTH_SHORT).show();
                        //restClient.testServer();
                        break;
                    case "Load nearby data" :
                        mDrawerLayout.closeDrawers();
                        Toast.makeText(MapsActivity.this, "Loading data from server", Toast.LENGTH_SHORT).show();
                        loadNearbyData();
                        break;
                    case "Synchronize with server" :
                        Toast.makeText(MapsActivity.this, "Uploading tracks to server", Toast.LENGTH_SHORT).show();
                        synchronizeFiles();
                        break;
                    default:
                        mDrawerLayout.closeDrawers();
                        currentTrack = getTrackWithName(item);
                        loadTrack(currentTrack);
                }
            }
        });
    }

    private TrackInfo getTrackWithName(String name){
        for (TrackInfo track: tracks){
            if (track.getName().equals(name)){
                return track;
            }
        }
        return null;
    }


    private void loadNearbyData() {
        Log.i(TAG,"Checking data for current location");
        restClient.loadData(locationManager.getCurrentLocation());
    }

    private void synchronizeFiles(){
        if (currentTrack.isUploaded()){
            Toast.makeText(MapsActivity.this, "Track already uploaded", Toast.LENGTH_SHORT).show();
        } else {
            restClient.uploadTrack(currentTrack);
        }

    }

    private void initSidepanelItems(){
        items = new ArrayList<>();
        items.add("Test Server");
        items.add("Load nearby data");
        items.add("Synchronize with server");
        for (TrackInfo track : tracks){
            items.add(track.getName());
        }
        String[] array = items.toArray(new String[0]);
        ArrayAdapter<String> mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,array);
        mDrawerList.setAdapter(mAdapter);


    }



    // User Actions
    public void record(View view) {
        int newTrackId = lastTrack == null ? 1 : lastTrack.getId()+1;
        if (!toggleButton.isChecked()){
            // stop recording
            accelerometerManager.stopRecording();
            locationManager.stopRecording();
            eventsManager.stopRecording();
            storageManager.resetData();
            lastTrack = new TrackInfo(newTrackId,currentCity);
            currentTrack = lastTrack;
            tracks.add(lastTrack);
            saveTracksFile();
            //restClient.uploadTrack(lastTrack);

            //update UI
            cancelButton.setVisibility(View.INVISIBLE);
            saveRawDataCheckbox.setEnabled(true);
            initSidepanelItems();
            loadLastTrack();
        } else {
            // start recording
            //update UI
            mMap.clear();
            accuracyLevelText.setVisibility(View.INVISIBLE);
            boolean saveRawData = saveRawDataCheckbox.isChecked();
            saveRawDataCheckbox.setEnabled(false);
            cancelButton.setVisibility(View.VISIBLE);
            //set storage info
            accelerometerManager.setSaveRawData(saveRawData);
            //update track info

            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(LAST_TRACK, newTrackId);
            editor.commit();

            //start recording
            Log.i(TAG,"Starting location updates.");
            eventsManager.startRecording();
            accelerometerManager.startRecording(newTrackId);
            locationManager.startRecording(newTrackId);
        }
    }


    public void cancel(View view){
        Log.i(TAG,"Cancelling location updates.");
        accelerometerManager.cancelRecording();
        locationManager.cancelRecording();

        //update UI
        cancelButton.setVisibility(View.INVISIBLE);
        toggleButton.setChecked(false);
        storageManager.resetData();
        saveRawDataCheckbox.setEnabled(true);
    }

    // UI Info




    private void setCurrentAxis(int axis){
        TextView xAxis = (TextView) findViewById(R.id.currentAxis);
        String axisLetter = axis == 0 ? "X" : (axis == 1 ? "Y" : "Z");
        xAxis.setText("Axis: " + axisLetter);
    }

    private void setCurrentValues(float[] delta){
        TextView xAxis = (TextView) findViewById(R.id.xAxis);
        xAxis.setText("Axis X: " + Float.toString(delta[0]));
        TextView yAxis = (TextView)findViewById(R.id.yAxis);
        yAxis.setText("Axis Y: " + Float.toString(delta[1]));
        TextView zAxis = (TextView)findViewById(R.id.zAxis);
        zAxis.setText("Axis Z: " + Float.toString(delta[2]));
    }

    public void showLocation(double lat,double lng){
        LatLng coordinate = new LatLng(lat, lng);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(coordinate)      // Sets the center of the map to Mountain View
                .zoom(14)                   // Sets the zoom
                .build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private void loadLastTrack() {
        if (lastTrack != null){
            loadTrack(lastTrack);
        }

    }

    private void loadTrack(TrackInfo selectedTrack){
        mMap.clear();

        List<String> trackData = storageManager.read(selectedTrack.getName());
        if (trackData == null || trackData.size() < 2){
            Log.i(TAG,"Track "+selectedTrack.getId()+" not found");
            Toast.makeText(MapsActivity.this, "Track "+selectedTrack.getId()+" not found", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(MapsActivity.this, "Showing track "+selectedTrack.getId(), Toast.LENGTH_SHORT).show();
        Log.i(TAG,"Loading track: "+selectedTrack.getId()+ " with "+trackData.size()+ " records.");
        List<EventsLocationRange> track = eventsManager.parseTrack(trackData);

        BitmapDescriptor trackMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA);

        PolylineOptions options = new PolylineOptions();
        options.width( 5 );
        options.visible( true );
        //set starting position
        Location lastLocation = track.get(0).getStartLocation();
        LatLng lastPosition = LocationManager.getLatLng(lastLocation);
        options.add(lastPosition);
        int lastColor = -1;
        mMap.addMarker(new MarkerOptions().position(lastPosition).title("Track Start").icon(trackMarker));
        showLocation(lastPosition.latitude,lastPosition.longitude);
        scale = new EventScoreScale(track);
        for (EventsLocationRange e : track){
            Location nextLocation = EventsManager.getNextLocation(e,lastLocation);
            LatLng nextPosition = LocationManager.getLatLng(nextLocation);
            int newColor = scale.getColor(e.getScore());
            if (lastColor != newColor) {
                if (lastColor != -1){
                    mMap.addPolyline( options );
                    options =  new PolylineOptions();
                    options.width( 5 );
                    options.visible( true );
                    options.add(lastPosition);
                }
                options.color(newColor);
            }
            options.add(nextPosition);
            lastColor = newColor;
            lastLocation = nextLocation;
            lastPosition = nextPosition;

        }
        mMap.addMarker(new MarkerOptions().position(lastPosition).title("Track End").icon(trackMarker));
        mMap.addPolyline( options );
    }

    public void showNearbyEvents(int accuracyLevel){

        List<EventsLocationRange> knownEvents = eventsManager.getNearbyEvents();
        if (knownEvents == null || knownEvents.size() == 0){
            return;
        }
        accuracyLevelText.setVisibility(View.VISIBLE);
        scale = new EventScoreScale(knownEvents);
        mMap.clear();
        for(EventsLocationRange e: knownEvents){
            if (e.getAccuracy() < accuracyLevel){
                continue;
            }
            PolylineOptions options = new PolylineOptions();
            options.width( 5 );
            options.visible( true );
            LatLng p1 = new LatLng(e.getStartLocation().getLatitude(),e.getStartLocation().getLongitude());
            LatLng p2 = new LatLng(e.getEndLocation().getLatitude(),e.getEndLocation().getLongitude());

            options.add(p1);
            options.add(p2);
            double score = e.getScore();
            Log.i(TAG,"ID "+e.getId());
            Log.i(TAG,"Start "+p1.latitude+ ","+p1.longitude+" End "+p2.latitude+ ","+p2.longitude+" score "+e.getScore());
            options.color(scale.getColor(score));
            Location midpoint = locationManager.getMidPoint(e.getStartLocation(),e.getEndLocation());
            LatLng position = new LatLng(midpoint.getLatitude(),midpoint.getLongitude());
            Log.i(TAG,"Showing event at "+position.latitude+ ","+position.longitude);
            // Showing range separator
            mMap.addMarker(new MarkerOptions()
                    .position(p2).anchor(0.5f, 0.5f)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.mapmarker)));
            // Showing section with information
            if (e.getAccuracy() > 1){
                String legend = "ID: "+e.getId()+", Score: "+e.getScore()+ ", Acc. "+e.getAccuracy();
                mMap.addMarker(new MarkerOptions()
                        .position(position).title(scale.getTag(score))
                        .icon(scale.getMarker(score))
                        .snippet(legend));
            }

            mMap.addPolyline( options );
        }
    }

    // Map Ready Event
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        enableMyLocation();
    }

    public void enableMyLocation(){
        if (mMap == null){
            return;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            loadLastTrack();
            locationManager.getmFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got lastTrack known location. In some rare situations this can be null.
                            Log.i(TAG,"Start location found.");
                            if (location != null) {
                                showLocation(location.getLatitude(),location.getLongitude());
//                                restClient.getCity(location.getLatitude(),location.getLongitude());
                                locationManager.setCurrentLocation(location);
                            }
                        }
                    });
        }
    }

    public void setCurrentCity(String currentCity){
        this.currentCity = currentCity;
    }

    // Permissions Handling
    public void requestPermissions(){
        Log.i(TAG,"Requesting permissions.");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.WRITE_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions(this,
                permissions,
                PERMISSIONS_REQUEST_CODE);
    }

    public boolean canWriteToSD(){
        if (ContextCompat.checkSelfPermission(MapsActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "All permissions granted.");
                    enableMyLocation();
                } else {
                    Log.e(TAG, "Permissions denied.");
                }
                break;
        }
        return;
    }


    // Accelerometer Sensor Events
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            accelerometerManager.updateData(event);
            setCurrentAxis(accelerometerManager.getAxis());
            setCurrentValues(accelerometerManager.getDelta());
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register this class as a listener for the orientation and
        // accelerometer sensors
//        sensorManager.registerListener(this,
//                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
//                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        // unregister listener
        super.onPause();
//        sensorManager.unregisterListener(this);
    }
}
