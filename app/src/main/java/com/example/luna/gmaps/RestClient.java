package com.example.luna.gmaps;

import android.location.Location;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by luna on 1/30/18.
 */

public class RestClient {

    private MapsActivity parent;
    private EventsManager eventsManager;

    public RestClient(MapsActivity parent,EventsManager eventsManager){
        this.parent = parent;
        this.eventsManager = eventsManager;
    }

    public void testServer(){
        try {
            String urlString = parent.getString(R.string.server_url)+parent.getString(R.string.test_url);
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                System.out.println(output);
                Toast.makeText(parent, output, Toast.LENGTH_LONG).show();
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }

    public void loadData(Location location){
        Log.i("RestClient","Loading data for "+location.getLatitude()+ "," + location.getLongitude());
        String url =parent.getString(R.string.server_url)+parent.getString(R.string.download_url);
        Ion.with(parent.getApplicationContext())
                .load(url)
                .setMultipartParameter("latitude",String.valueOf(location.getLatitude()))
                .setMultipartParameter("longitude",String.valueOf(location.getLongitude()))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        System.out.println(result);
                        if (result == null){
                            Log.i("RestClient","No answer from server");
                        } else {
                            eventsManager.parseNearbyEvents(result);

                        }

                    }
                });
    }

    public void uploadTrack(final TrackInfo track){
        String url =parent.getString(R.string.server_url)+parent.getString(R.string.upload_url);
        File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/baches2017",
                track.getName()+".csv");
        Ion.with(parent.getApplicationContext())
                .load(url)
                .setMultipartFile("track", "text/csv", file)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result != null){
                            track.setUploaded(true);
                            parent.saveTracksFile();
                            Toast.makeText(parent, "Track "+track.getId()+" uploaded.", Toast.LENGTH_LONG).show();
                        } else {
                            System.out.println("No answer from server");
                            Toast.makeText(parent, "No answer from server", Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    public boolean isInternetAvailable() {
        try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");
        } catch (Exception e) {
            return false;
        }
    }

}
