package com.example.luna.gmaps;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;

/**
 * Created by luna on 10/17/17.
 */

public class AccelerometerManager {

    private static final String TAG = "Accelerometer Manager";
    private static final double AXISTHRESHOLD = 2;
    private static final double NOISETHRESHOLD = 2;
    public static final int STBTOLERANCE = 5;

    private int trackId;

    private MapsActivity parent;
    private StorageManager storageManager;
    private SensorManager sensorManager;
    private EventsManager eventsManager;

    private float[] raw = new float[3];
    private float[] delta = new float[3];
    private float[] diff = new float[3];

    private int locationUpdatesCounter = 0;
    private int axis;
    private int secondaryAxis;
    private int forwardAxis;
    private int stabilizing;

    private boolean saveRawData = false;
    private boolean recording = false;


    public AccelerometerManager(MapsActivity parent,StorageManager storageManager,EventsManager eventsManager){
        this.parent = parent;
        this.eventsManager = eventsManager;
        this.sensorManager = (SensorManager) parent.getSystemService(parent.SENSOR_SERVICE);
        this.storageManager = storageManager;
    }

    public void startRecording(int trackId){
        this.trackId = trackId;
        stabilizing = STBTOLERANCE;
        locationUpdatesCounter = 0;

        sensorManager.registerListener(parent,
                sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);
        recording = true;
    }

    public void stopRecording(){
        if (saveRawData){
            storageManager.write("accelerometer"+trackId);
        }
        cancelRecording();
    }

    public void cancelRecording(){
        sensorManager.unregisterListener(parent);
        recording = false;
    }


    public void setSaveRawData(boolean saveRawData) {
        this.saveRawData = saveRawData;
    }

    public float[] getDelta() {
        return delta;
    }

    public int getAxis() {
        return axis;
    }

    public void updateData(SensorEvent e){
        float[] values = e.values;
        // Movement
        // update values
        checkAxis();
        diff[0] = raw[0] - values[0];
        diff[1] = raw[1] - values[1];
        diff[2] = raw[2] - values[2];
        if (diff[0] == 0 && diff[1] == 0 && diff[2] == 0){
            return;
        }
        delta[0] = filterNoise(Math.abs(diff[0]));
        delta[1] = filterNoise(Math.abs(diff[1]));
        delta[2] = filterNoise(Math.abs(diff[2]));
        raw[0] = values[0];
        raw[1] = values[1];
        raw[2] = values[2];
        // check for event
        if (delta[axis]>0 || delta[secondaryAxis] > 0 || eventsManager.eventIsOpen()){
            if (recording){
                eventsManager.checkEvent(delta,isStable());
                if (!eventsManager.eventIsOpen()){
                    stabilizing = STBTOLERANCE;
                }
            }
        }
        // save raw data
        if (recording && saveRawData){
            String axisLetter = axis == 0 ? "X" : (axis == 1 ? "Y" : "Z");
            String accData = DataParser.convertAccelerometerData(raw,delta,diff,axisLetter,locationUpdatesCounter,eventsManager.getEventId());
            storageManager.addData("accelerometer"+trackId,accData);
        }
    }

    private float filterNoise(float value){
        return value < NOISETHRESHOLD ? 0 : value;
    }

    private void checkAxis(){
        if (checkThreshold(raw[0],raw[1],raw[2])){
            axis = 0;
            secondaryAxis = 2;
            forwardAxis = 1;
        }
        if (checkThreshold(raw[1],raw[0],raw[2])){
            axis = 1;
            secondaryAxis = 0;
            forwardAxis = 2;
        }
        if (checkThreshold(raw[2],raw[0],raw[1])){
            axis = 2;
            secondaryAxis = 0;
            forwardAxis = 1;
        }
    }

    private boolean checkThreshold(double v1, double v2, double v3){
        return Math.abs(v1 - SensorManager.GRAVITY_EARTH)<AXISTHRESHOLD && (v2 < AXISTHRESHOLD) && (v3 < AXISTHRESHOLD);
    }

    public boolean isStable(){
        if (delta[0] == 0 && delta[1] == 0 && delta[2] == 0 && checkThreshold(raw[axis],raw[secondaryAxis],raw[forwardAxis])){
            stabilizing--;
        } else {
            stabilizing = STBTOLERANCE;
        }
        return stabilizing<=0;
    }

    public void setLocationUpdatesCounter(int locationUpdatesCounter){
        this.locationUpdatesCounter = locationUpdatesCounter;
    }

}
