package com.example.luna.gmaps;

import android.graphics.Color;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.util.Collection;

/**
 * Created by luna on 12/7/17.
 */

public class EventScoreScale {

    public static final BitmapDescriptor MARKER_NOEVENT = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN);
    public static final BitmapDescriptor MARKER_VERYLOW = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
    public static final BitmapDescriptor MARKER_LOW = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
    public static final BitmapDescriptor MARKER_MEDIUM = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
    public static final BitmapDescriptor MARKER_HIGH = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
    public static final BitmapDescriptor MARKER_VERYHIGH = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET);

    public static final int COLOR_NOEVENT = Color.parseColor( "#00ffff" ); //cyan
    public static final int COLOR_VERYLOW = Color.parseColor( "#00ff00" ); //green
    public static final int COLOR_LOW = Color.parseColor( "#ffff00" ); // yellow
    public static final int COLOR_MEDIUM = Color.parseColor( "#ff8000" ); //orange
    public static final int COLOR_HIGH = Color.parseColor( "#ff0000" );
    public static final int COLOR_VERYHIGH = Color.parseColor( "#8000ff" ); //purple

    private double veryLowLimit;
    private double lowLimit;
    private double mediumLimit;
    private double highLimit;

    public EventScoreScale(Collection<? extends Scorable> scores){
        double min = 100000;
        double max = 0;
        double avg = 0;
        int counter = 0;
        for(Scorable s: scores){
            double score = s.getScore();
            if (score > 0){
                counter++;
                avg+=score;
                min = Math.min(min,score);
                max = Math.max(max,score);
            }
        }
        avg = avg/counter;
        System.out.println("min "+min);
        System.out.println("max "+max);
        System.out.println("avg "+avg);
        double lowerStep = (avg-min)/4;
        double upperStep = (max-avg)/4;
        System.out.println("lowerStep "+lowerStep);
        System.out.println("upperStep "+upperStep);
        veryLowLimit = min+lowerStep;
        System.out.println("veryLowLimit "+veryLowLimit);
        lowLimit = avg-lowerStep;
        System.out.println("lowlimit "+lowLimit);
        mediumLimit = avg + upperStep;
        System.out.println("mediumLimit "+mediumLimit);
        highLimit = max - upperStep;
        System.out.println("highLimit "+highLimit);
    }

    public int getColor(double score){
        if (score == 0){
            return COLOR_NOEVENT;
        }
        if (score < veryLowLimit) {
            return COLOR_VERYLOW;
        }
        if (score < lowLimit) {
            return COLOR_LOW;
        }
        if (score < mediumLimit){
            return COLOR_MEDIUM;
        }
        if (score < highLimit){
            return COLOR_HIGH;
        }
        return COLOR_VERYHIGH;
    }

    public BitmapDescriptor getMarker(double score){
        if (score == 0){
            return MARKER_NOEVENT;
        }
        if (score < veryLowLimit) {
            return MARKER_VERYLOW;
        }
        if (score < lowLimit) {
            return MARKER_LOW;
        }
        if (score < mediumLimit){
            return MARKER_MEDIUM;
        }
        if (score < highLimit){
            return MARKER_HIGH;
        }
        return MARKER_VERYHIGH;
    }

    public String getTag(double score){
        if (score == 0){
            return "No events";
        }
        if (score < veryLowLimit) {
            return "Very low event";
        }
        if (score < lowLimit) {
            return "Low event";
        }
        if (score < mediumLimit){
            return "Medium event";
        }
        if (score < highLimit){
            return "High event";
        }
        return "Very High event";
    }

}
