package com.example.luna.gmaps;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luna on 12/6/17.
 */

public class EventsLocationRange implements Scorable{

    private String id;

    private List<StabilityEvent> events = new ArrayList<>();
    private double score;
    private Long readingDateTime;
    private int accuracy = 1;
    private Location startLocation;
    private Location endLocation;

    public EventsLocationRange(String id,Location startLocation, Location endLocation){
        this.id = id;
        this.readingDateTime = System.currentTimeMillis();
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.score = -1;
    }

    public EventsLocationRange(Location startLocation, Location endLocation){
        this.readingDateTime = System.currentTimeMillis();
        this.startLocation = startLocation;
        this.endLocation = endLocation;
        this.score = -1;
    }

    public String getId() {
        return id;
    }

    public Location getStartLocation() {
        return startLocation;
    }

    public Location getEndLocation() {
        return endLocation;
    }

    public float getRangeDistance(){
        return startLocation.distanceTo(endLocation);
    }

    public void addEvent(StabilityEvent e){
        if (events == null){
            events = new ArrayList<>();
        }
        events.add(e);
    }

    public void setEvents(List<StabilityEvent> events) {
        this.events = events;
    }
    public List<StabilityEvent> getEvents() {
        return events;
    }

    public double getScore(){
        if (score != -1){
            return score;
        }
        float score = 0;
        for (StabilityEvent event : events){
            score += event.getScore();
        }
        return score;
    }

    public void setScore(double score){
        this.score = score;
    }

    public void setStartLocation(Location startLocation) {
        this.startLocation = startLocation;
    }

    public void setEndLocation(Location endLocation) {
        this.endLocation = endLocation;
    }

    public Long getReadingDateTime() {
        return readingDateTime;
    }

    public void setReadingDateTime(Long readingDateTime) {
        this.readingDateTime = readingDateTime;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }
}
